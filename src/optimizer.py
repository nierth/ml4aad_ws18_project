import torch.optim as optim
import torch.nn as nn
import torch
import numpy as np

class Optimizer():

    def __init__(self, model_params, class_frequency, cfg):

        self.optimizer = None
        self.scheduler = None
        self.loss = None

        optimizer_type = cfg["optimizer_type"]

        if optimizer_type == "Adadelta":
            self.optimizer = optim.Adadelta(model_params, lr=cfg["optimizer_lr"], weight_decay=cfg["optimizer_weight_decay"])
        elif optimizer_type == "Adagrad":
            self.optimizer = optim.Adagrad(model_params, lr=cfg["optimizer_lr"], weight_decay=cfg["optimizer_weight_decay"])
        elif optimizer_type == "Adam":
            self.optimizer = optim.Adam(model_params, lr=cfg["optimizer_lr"], weight_decay=cfg["optimizer_weight_decay"])
        elif optimizer_type == "SparseAdam":
            self.optimizer = optim.SparseAdam(model_params, lr=cfg["optimizer_lr"], weight_decay=cfg["optimizer_weight_decay"])
        elif optimizer_type == "Adamax":
            self.optimizer = optim.Adamax(model_params, lr=cfg["optimizer_lr"], weight_decay=cfg["optimizer_weight_decay"])
        elif optimizer_type == "ASGD":
            self.optimizer = optim.ASGD(model_params, lr=cfg["optimizer_lr"], weight_decay=cfg["optimizer_weight_decay"])
        elif optimizer_type == "RMSprop":
            self.optimizer = optim.RMSprop(model_params, lr=cfg["optimizer_lr"], weight_decay=cfg["optimizer_weight_decay"])
        elif optimizer_type == "SGD":
            self.optimizer = optim.SGD(model_params, lr=cfg["optimizer_lr"], weight_decay=cfg["optimizer_weight_decay"])
        else:
            raise ValueError("Unknown parameter for optimizer_type: ", optimizer_type)

        # calculate correct weight
        class_frequency = np.asarray(class_frequency)
        class_weight = class_frequency / np.mean(class_frequency)

        self.loss = nn.CrossEntropyLoss()
        #self.loss = nn.CrossEntropyLoss(weight=torch.from_numpy(class_weight).float())
        self.scheduler = optim.lr_scheduler.ExponentialLR(self.optimizer, gamma=cfg["optimizer_lr_gamma"])

    def getOptimizer(self):
        return self.optimizer

    def getLoss(self):
        return self.loss

    def getScheduler(self):
        return self.scheduler
