import ConfigSpace as CS
import ConfigSpace.hyperparameters as CSH

# get "default" configuration for testing purpose
def get_configuration():
       cfg = {
              # directory used for logging
              "general_log_dir": "./logs/",
              # directory of the underlying data
              "general_data_dir": "../data",
              # number of BOHB iterations
              "bohb_iterations": 10,
              # minimal budget for a BOHB run
              "bohb_min_budget": 0.1,
              # maximal budget for a BOHB run
              "bohb_max_budget": 10,
              # minimal points to train the model
              "bohb_min_points": 100,
              # random fraction of all evaluated BOHB runs
              "bohb_random_fraction": 0.5,
              # BOHB splitting factor
              "bohb_eta": 3,
              #  KMNIST / K49
              "train_dataset": "KMNIST",
              # batchsize of each training epoch
              "train_batch_size": 50,
              # how many percent of the training data are used for validation
              "train_validation_split": 0.3,
              # how many linear parameters the model may contain at most
              "train_max_linear_params": 1e8,
              # how many other parameters the model may contain at most
              "train_max_other_params": 5e5,
              # normalized time per batch
              "train_normalized_max_time_per_batch": 5,
              # number of training epochs
              "train_epochs": 10,
              # optimizer type
              "optimizer_type": "RMSprop",
              # learning rate of the optimizer
              "optimizer_lr": 0.001,
              # learning rate scheduler
              "optimizer_lr_gamma": 1,
              # L2 regularization
              "optimizer_weight_decay": 0.001,
              # do data augmentation
              "augmentation_ratio": 0.01,
              # maximum rotation (deg) for data augmentation
              "augmentation_intensity": 1,
              # number of major blocks
              "model_blocks": 2,
              # number of minor blocks
              "model_depth": [1,1],
              # type of the major model blocks
              "model_block_types": [2,4],
              # number of CNN planes per block (multiplied by 4)
              "model_planes": [1,1],
              # stride per block
              "model_stride": [1,6],
              # dropout_rate
              "model_dropout": 0.001,
              # use batch normalization?
              "model_batchnorm": False,
              # add bias to convolutions
              "model_conv_bias": False,
              # reduce dimensionality before any fully connected layers?
              "model_avg_pool": False,
              # ReLU / Sigmoid / Tanh
              "model_activation": "ReLU"
              }
       return cfg

# config for default:
# blocks = 2,
# depth = [1,1],
# block_types = [2,4]
# planes = [1,1]
# stride = [1,6]
# batchnorm = False
# conv_bias = False
# avg_pool = False

def get_configspace():
    config_space = CS.ConfigurationSpace()

    # training parameters
    config_space.add_hyperparameter(CSH.UniformIntegerHyperparameter(name='train_batch_size', lower=30, upper=300, log=True))

    # optimizer parameters
    config_space.add_hyperparameter(CSH.CategoricalHyperparameter('optimizer_type', choices=['Adam', 'RMSprop']))
    config_space.add_hyperparameter(CSH.UniformFloatHyperparameter(name='optimizer_lr', lower=1e-4, upper=1e-2, log=True))
    config_space.add_hyperparameter(CSH.UniformFloatHyperparameter(name='optimizer_lr_gamma', lower=0.7, upper=1, log=False))
    config_space.add_hyperparameter(CSH.UniformFloatHyperparameter(name='optimizer_weight_decay', lower=1e-6, upper=1e-3, log=True))

    # data augmentation parameters
    config_space.add_hyperparameter(CSH.UniformFloatHyperparameter(name='augmentation_ratio', lower=1e-4, upper=5e-1, log=True))
    config_space.add_hyperparameter(CSH.UniformFloatHyperparameter(name='augmentation_intensity', lower=1e-4, upper=3, log=True))

    # number of model blocks
    lower_blocks = 1
    upper_blocks = 3
    model_blocks = CSH.UniformIntegerHyperparameter('model_blocks', lower=lower_blocks, upper=upper_blocks, log=False)
    config_space.add_hyperparameter(model_blocks)

    # general parameters
    model_batchnorm = CSH.CategoricalHyperparameter('model_batchnorm', [False, True])
    model_conv_bias = CSH.CategoricalHyperparameter('model_conv_bias', [False, True])
    model_avg_pool = CSH.CategoricalHyperparameter('model_avg_pool',  [False])
    model_activation_type = CSH.CategoricalHyperparameter('model_activation', ['ReLU'])
    model_dropout = CSH.UniformFloatHyperparameter(name='model_dropout', lower=1e-3, upper=0.3, log=True)

    config_space.add_hyperparameter(model_batchnorm)
    config_space.add_hyperparameter(model_conv_bias)
    config_space.add_hyperparameter(model_avg_pool)
    config_space.add_hyperparameter(model_activation_type)
    config_space.add_hyperparameter(model_dropout)

    lower_block_type = 0
    upper_block_type = 11
    model_planes = [1, 2, 4, 8, 12, 16, 20]
    model_stride = [1, 2, 4, 6]
    model_depth = [1, 2, 3]

    model_block_type_list = []
    model_planes_list = []
    model_stride_list = []
    model_depth_list = []

    for i in range(upper_blocks):
        model_block_type_list.append(CSH.UniformIntegerHyperparameter('model_block_type_' + str(i), lower=lower_block_type, upper=upper_block_type, log=False))
        model_planes_list.append(CSH.CategoricalHyperparameter('model_planes_' + str(i), choices=model_planes))
        model_stride_list.append(CSH.CategoricalHyperparameter('model_stride_' + str(i), choices=model_stride))
        model_depth_list.append(CSH.CategoricalHyperparameter('model_depth_' + str(i), choices=model_depth))

        config_space.add_hyperparameter(model_block_type_list[-1])
        config_space.add_hyperparameter(model_planes_list[-1])
        config_space.add_hyperparameter(model_stride_list[-1])
        config_space.add_hyperparameter(model_depth_list[-1])

        if i > 0:
            config_space.add_condition(CS.GreaterThanCondition(model_block_type_list[-1], model_blocks, i))
            config_space.add_condition(CS.GreaterThanCondition(model_planes_list[-1], model_blocks, i))
            config_space.add_condition(CS.GreaterThanCondition(model_stride_list[-1], model_blocks, i))
            config_space.add_condition(CS.GreaterThanCondition(model_depth_list[-1], model_blocks, i))

    return config_space


def map_config_space_to_configuration(cfg, config):
    cfg["train_batch_size"]       = config["train_batch_size"]

    cfg["optimizer_lr"]           = config["optimizer_lr"]
    cfg["optimizer_lr_gamma"]     = config["optimizer_lr_gamma"]
    cfg["optimizer_weight_decay"] = config["optimizer_weight_decay"]
    cfg["optimizer_type"]         = config["optimizer_type"]

    cfg["augmentation_ratio"]     = config["augmentation_ratio"]
    cfg["augmentation_intensity"] = config["augmentation_intensity"]

    cfg["model_batchnorm"]  = config["model_batchnorm"]
    cfg["model_conv_bias"]  = config["model_conv_bias"]
    cfg["model_avg_pool"]   = config["model_avg_pool"]
    cfg["model_activation"] = config["model_activation"]
    cfg["model_dropout"]    = config["model_dropout"]

    cfg["model_blocks"] = config["model_blocks"]

    cfg["model_block_types"] = []
    cfg["model_planes"] = []
    cfg["model_stride"] = []
    cfg["model_depth"] = []

    for i in range(config["model_blocks"]):
        if 'model_block_type_'+str(i) in config.keys(): cfg["model_block_types"].append(config["model_block_type_" + str(i)])
        if 'model_planes_'+str(i) in config.keys(): cfg["model_planes"].append(config["model_planes_" + str(i)])
        if 'model_stride_'+str(i) in config.keys(): cfg["model_stride"].append(config["model_stride_" + str(i)])
        if 'model_depth_'+str(i) in config.keys(): cfg["model_depth"].append(config["model_depth_" + str(i)])

    return cfg
