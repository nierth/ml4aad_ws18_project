import torch.nn as nn
import torch

import numpy as np


def update_sizes(cfg, index, in_size):
    stride = cfg["model_stride"][index]
    in_size = int(np.floor((in_size-1) / stride+1))
    in_planes = cfg["model_planes"][index]*4

    return in_size, in_planes


class Reshape(nn.Module):
    def __init__(self, *args):
        super(Reshape, self).__init__()
        self.shape = args

    def forward(self, x):
        return x.view(self.shape)


class Print(nn.Module):
    def __init__(self):
        super(Print, self).__init__()

    def forward(self, x):
        print(x.shape)
        return x


class Identity(nn.Module):
    def __init__(self):
        super(Identity, self).__init__()

    def forward(self, x):
        return x


class BatchNorm(nn.Module):
    def __init__(self, cfg, planes):
        super(BatchNorm, self).__init__()

        if cfg["model_batchnorm"] is True:
            self.batchnorm = nn.BatchNorm2d(planes)
        else:
            self.batchnorm = Identity()

    def forward(self, x):
        return self.batchnorm(x)


class Conv(nn.Module):
    def __init__(self, cfg, in_planes, planes, stride, kernel, padding):
        super(Conv, self).__init__()
        self.conv = nn.Conv2d(in_planes,
                              planes,
                              stride=stride,
                              kernel_size=kernel,
                              padding=padding,
                              bias=cfg["model_conv_bias"])

    def forward(self, x):
        return self.conv(x)


class Activation(nn.Module):
    def __init__(self, cfg):
        super(Activation, self).__init__()

        model_activation = cfg["model_activation"]
        if model_activation == "ReLU":
            self.activation = nn.ReLU()
        elif model_activation == "Sigmoid":
            self.activation = nn.Sigmoid()
        elif model_activation == "Tanh":
            self.activation = nn.Tanh()
        else:
            raise ValueError("Unknown parameter for model_activation: ", model_activation)

    def forward(self, x):
        return self.activation(x)

class DropoutBlock(nn.Module):
    def __init__(self, cfg):
        super(DropoutBlock, self).__init__()
        self.dropout = nn.Dropout(cfg["model_dropout"])

    def forward(self, x):
        return self.dropout(x)


class IdentityBlock(nn.Module):
    def __init__(self, cfg, index, in_planes):
        super(IdentityBlock, self).__init__()

        planes = cfg["model_planes"][index]*4
        stride = cfg["model_stride"][index]

        self.seq = Identity()
        if stride != 1 or in_planes != planes:
            self.seq = nn.Sequential(Conv(cfg, in_planes, planes, stride, 1, 0),
                                     BatchNorm(cfg, planes))

    def forward(self, x):
        return self.seq.forward(x)


class ConvBlock(nn.Module):
    def __init__(self, cfg, index, in_planes, kernel, dblock):
        super(ConvBlock, self).__init__()

        planes = cfg["model_planes"][index]*4
        stride = cfg["model_stride"][index] if dblock is False else 1
        in_planes = in_planes if dblock is False else planes
        padding = int(np.floor(kernel / 2))

        self.conv = Conv(cfg, in_planes, planes, stride, kernel, padding)
        self.bn = BatchNorm(cfg, planes)
        self.act = Activation(cfg)

    def forward(self, x):
        x = self.conv(x)
        x = self.bn(x)
        x = self.act(x)
        return x


class MaxPoolBlock(nn.Module):
    def __init__(self, cfg, index, in_planes, kernel, dblock):
        planes = cfg["model_planes"][index]*4
        stride = cfg["model_stride"][index] if dblock is False else 1
        in_planes = in_planes if dblock is False else planes
        padding = int(np.floor(kernel / 2))

        super(MaxPoolBlock, self).__init__()
        self.pool = nn.MaxPool2d(kernel_size=kernel, stride=stride, padding=padding)
        self.conv = Conv(cfg, in_planes, planes, 1, 1, 0)

    def forward(self, x):
        out = self.pool(x)
        out = self.conv(out)
        return out


class LinearBlock(nn.Module):
    def __init__(self, cfg, index, in_size, in_planes, dblock):
        super(LinearBlock, self).__init__()

        if dblock is False:
            out_size, out_planes = update_sizes(cfg, index, in_size)
        else:
            in_size, in_planes = update_sizes(cfg, index, in_size)
            out_size = in_size
            out_planes = in_planes

        self.blocks = []

        self.in_size = in_size
        self.in_planes = in_planes
        self.out_size = out_size
        self.out_planes = out_planes

        if cfg["model_avg_pool"] == True:
            self.blocks.append(nn.AdaptiveAvgPool2d((1, 1)))
            self.blocks.append(Reshape(-1, in_planes))
            self.blocks.append(nn.Linear(in_features=in_planes,
                                         out_features=out_planes*out_size**2))
        else:
            self.blocks.append(Reshape(-1, in_planes*in_size**2))
            self.blocks.append(nn.Linear(in_features=in_planes*in_size**2,
                                         out_features=out_planes*out_size**2))
        self.blocks.append(Reshape(-1, out_planes, out_size, out_size))
        self.blocks.append(BatchNorm(cfg, out_planes))
        self.blocks.append(Activation(cfg))
        self.seq = nn.Sequential(*self.blocks)

    def forward(self, x):
        return self.seq.forward(x)


class ResNetBlock(nn.Module):
    def __init__(self, cfg, index, in_planes, kernel1, kernel2, dblock):
        super(ResNetBlock, self).__init__()

        planes = cfg["model_planes"][index]*4
        stride = cfg["model_stride"][index] if dblock is False else 1
        in_planes = in_planes if dblock is False else planes

        padding1 = int(np.floor(kernel1 / 2))
        padding2 = int(np.floor(kernel2 / 2))

        self.downsample = None
        if stride != 1 or in_planes != planes:
            self.downsample = nn.Sequential(Conv(cfg, in_planes, planes, stride, 1, 0),
                                            BatchNorm(cfg, planes))

        self.bn1 = BatchNorm(cfg, in_planes)
        self.act1 = Activation(cfg)
        self.conv1 = Conv(cfg, in_planes, planes, stride, kernel1, padding1)
        self.bn2 = BatchNorm(cfg, planes)
        self.act2 = Activation(cfg)
        self.conv2 = Conv(cfg, planes, planes, 1, kernel2, padding2)

    def forward(self, x):
        identity = x

        out = self.bn1(x)
        out = self.act1(out)
        out = self.conv1(out)
        out = self.bn2(out)
        out = self.act2(out)
        out = self.conv2(out)

        if self.downsample is not None:
            identity = self.downsample(x)

        out += identity
        return out


class InceptionBlock(nn.Module):
    def __init__(self, cfg, index, in_planes, activate, dblock):
        super(InceptionBlock, self).__init__()

        planes = cfg["model_planes"][index]
        stride = cfg["model_stride"][index] if dblock is False else 1
        in_planes = in_planes if dblock is False else planes*4

        self.activate = activate

        self.conv1 = Conv(cfg, in_planes, planes, stride, 1, 0)
        self.conv3 = Conv(cfg, in_planes, planes, stride, 3, 1)
        self.conv5 = Conv(cfg, in_planes, planes, stride, 5, 2)
        self.maxpool = nn.MaxPool2d(3,stride=stride,padding=1)
        self.maxconv = Conv(cfg, in_planes, planes, 1, 1, 0)
        self.bn1 = BatchNorm(cfg, in_planes)
        self.bn2 = BatchNorm(cfg, planes*4)
        self.act1 = Activation(cfg)
        self.act2 = Activation(cfg)

    def forward(self, x):
        x = self.bn1(x)
        if self.activate:
            x = self.act1(x)

        c1 = self.conv1(x)
        c3 = self.conv3(x)
        c5 = self.conv5(x)
        mp = self.maxpool(x)
        mc = self.maxconv(mp)

        out = torch.cat([c1,c3,c5,mc],dim=1)

        out = self.bn2(out)
        if self.activate:
            out = self.act2(out)

        return out


class InceptionResnetBlock(nn.Module):
    def __init__(self, cfg, index, in_planes, activate, dblock):
        super(InceptionResnetBlock, self).__init__()

        planes = cfg["model_planes"][index]
        stride = cfg["model_stride"][index] if dblock is False else 1
        in_planes = in_planes if dblock is False else planes*4

        self.downsample = None
        if stride != 1 or in_planes != planes:
            self.downsample = nn.Sequential(Conv(cfg, in_planes, planes, stride, 1, 0),
                                            BatchNorm(cfg, planes))

        self.activate = activate

        self.conv1 = Conv(cfg, in_planes, planes, stride, 1, 0)
        self.conv3 = Conv(cfg, in_planes, planes, stride, 3, 1)
        self.conv5 = Conv(cfg, in_planes, planes, stride, 5, 2)
        self.maxpool = nn.MaxPool2d(3,stride=stride,padding=1)
        self.maxconv = Conv(cfg, in_planes, planes, 1, 1, 0)
        self.bn1 = BatchNorm(cfg, in_planes)
        self.bn2 = BatchNorm(cfg, planes*4)
        self.act1 = Activation(cfg)
        self.act2 = Activation(cfg)

    def forward(self, x):
        x = self.bn1(x)
        if self.activate:
            x = self.act1(x)

        identity = x
        if self.downsample is not None:
            identity = self.downsample(x)

        c1 = self.conv1(x)
        c3 = self.conv3(x)
        c5 = self.conv5(x)
        mp = self.maxpool(x)
        mc = self.maxconv(mp)
        c1 += identity
        c3 += identity
        c5 += identity
        mc += identity

        out = torch.cat([c1,c3,c5,mc],dim=1)

        out = self.bn2(out)
        if self.activate:
            out = self.act2(out)

        return out


class Net(nn.Module):
    def __init__(self, cfg, num_classes):
        super(Net, self).__init__()

        self.in_size = 28   # initial size of the images (hardcoded)
        self.in_planes = 1  # initial dimension of the images (hardcoded)
        self.blocks = []    # list of all blocks

        if cfg["model_blocks"] > 0:
            for i in range(cfg["model_blocks"]):
                self._make_block(cfg, i)
                self.in_size, self.in_planes = update_sizes(cfg, i, self.in_size)
        self._make_final_block(cfg, num_classes)
        self.seq = nn.Sequential(*self.blocks)

    def _make_block(self, cfg, index):
        block_type  = cfg["model_block_types"][index]
        block_depth = cfg["model_depth"][index]

        block_types = ["LinearBlock(cfg, index, self.in_size, self.in_planes, ",
                       "ConvBlock(cfg, index, self.in_planes, 3, ",
                       "ConvBlock(cfg, index, self.in_planes, 5, ",
                       "MaxPoolBlock(cfg, index, self.in_planes, 3, ",
                       "MaxPoolBlock(cfg, index, self.in_planes, 5, ",
                       "ResNetBlock(cfg, index, self.in_planes, 3, 3, ",
                       "ResNetBlock(cfg, index, self.in_planes, 5, 5, ",
                       "ResNetBlock(cfg, index, self.in_planes, 7, 7, ",
                       "InceptionBlock(cfg, index, self.in_planes, False, ",
                       "InceptionBlock(cfg, index, self.in_planes, True, ",
                       "InceptionResnetBlock(cfg, index, self.in_planes, False, ",
                       "InceptionResnetBlock(cfg, index, self.in_planes, True, "]

        strng1 = block_types[block_type] + "False)"
        self.blocks.append(eval(strng1))
        self.blocks.append(DropoutBlock(cfg))
        for i in range(block_depth - 1):
            strng2 = block_types[block_type] + "True)"
            self.blocks.append(eval(strng2))
            self.blocks.append(DropoutBlock(cfg))

    def _make_final_block(self, cfg, num_classes):
        if cfg["model_avg_pool"] == True:
            self.blocks.append(nn.AdaptiveAvgPool2d((1, 1)))
            self.blocks.append(Reshape(-1, self.in_planes))
            self.blocks.append(nn.Linear(in_features=self.in_planes,
                                         out_features=num_classes))
        else:
            self.blocks.append(Reshape(-1, self.in_planes*self.in_size**2))
            self.blocks.append(nn.Linear(in_features=self.in_planes*self.in_size**2,
                                         out_features=num_classes))

    def forward(self, x):
        #return self.seq.forward(x)
        for elem in self.seq:
            x = elem(x)
        return x


def get_model_parameters(model):
    params_linear = 0
    params_other = 0

    for elem in model.modules():
        if hasattr(elem, "weight") and hasattr(elem.weight, "size"):
            if isinstance(elem, nn.Linear):
                params_linear += torch.prod(torch.LongTensor(list(elem.weight.size())))
            else:
                params_other += torch.prod(torch.LongTensor(list(elem.weight.size())))

        if hasattr(elem, "bias") and hasattr(elem.bias, "size"):
            if isinstance(elem, nn.Linear):
                params_linear += torch.prod(torch.LongTensor(list(elem.bias.size())))
            else:
                params_other += torch.prod(torch.LongTensor(list(elem.bias.size())))

    return params_linear, params_other