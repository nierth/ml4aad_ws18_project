import time
import torch
import logging
import os
import configuration
import pprint

from torchsummary import summary
from sklearn.metrics import balanced_accuracy_score

import numpy as np
import datasets as dts
import optimizer as opt
import net

device = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')
#device = torch.device('cpu')
deviceCpu = torch.device('cpu')

def eval(model, loader, device, type='unknown', fraction=1):
    true, pred = [], []
    with torch.no_grad():  # no gradient needed
        for i_batch, (images, labels)  in enumerate(loader):
            if i_batch > len(loader) * fraction:
                break
            images = images.to(device)
            labels = labels.to(device)
            outputs = model(images)
            _, predicted = torch.max(outputs.data, 1)
            true.extend(labels.to(deviceCpu))
            pred.extend(predicted.to(deviceCpu))
        # return balanced accuracy where each sample is weighted according to occurrence in dataset
        score = balanced_accuracy_score(true, pred)
        logging.info('#' * 120)
        logging.info('{0} Accuracy of the model on {1} images: {2}%'.format(type, len(true), 100 * score))
        logging.info('#' * 120)
    return score


def train(cfg):
    # print resulting configuration
    logging.debug(pprint.pformat(cfg))

    # Prepare data, load new model
    data = dts.Datasets(cfg)
    aug = dts.Augmentation(cfg)

    train_loader, valid_loader, test_loader = data.getLoaders()
    num_classes = train_loader.dataset.n_classes

    model = net.Net(cfg=cfg, num_classes=num_classes)
    model = model.to(device)


    optim = opt.Optimizer(model.parameters(), train_loader.dataset.class_frequency, cfg)
    lossCrit = optim.getLoss().to(device)
    optimizer = optim.getOptimizer()
    scheduler = optim.getScheduler()

    # early out if model is too large
    params_linear, params_other = net.get_model_parameters(model)
    logging.info('Parameters: {}, {}'.format(params_linear, params_other))
    if params_linear > cfg["train_max_linear_params"] or \
        params_other > cfg["train_max_other_params"]:
        logging.info('ooo' * 40)
        logging.info('model too large, return... {}, {}'.format(params_linear, params_other))
        logging.info('ooo' * 40)
        return earlyOut("model too large")

    #logging.info('Generated Network:')
    #summary(model, (train_loader.dataset.channels, train_loader.dataset.img_rows, train_loader.dataset.img_cols), device='cpu')

    batch_size = cfg["train_batch_size"]
    normalized_max_time_per_batch = cfg["train_normalized_max_time_per_batch"]

    epoch_times = []
    total_step = len(train_loader)

    # Account for values smaller than a single training epoch
    epochs_num  = int(np.ceil(cfg["train_epochs"]))
    epochs_frac = cfg["train_epochs"]%1

    model.train()

    train_time = time.time()
    for epoch in range(epochs_num):
        scheduler.step(int(epoch))

        epoch_start_time = time.time()
        for i_batch, (images, labels) in enumerate(train_loader):
            if i_batch > len(train_loader) * epochs_frac and epoch == epochs_num-1:
                break

            images = aug.augmentInputData(images)
            #labels = aug.augmentOutputData(labels, num_classes)

            images = images.to(device)
            labels = labels.to(device)

            outputs = model(images)

            loss = lossCrit(outputs, labels)
            optimizer.zero_grad()
            loss.backward()

            optimizer.step()

            # early out if model training takes too long
            nomalized_time_per_batch = (time.time() - train_time) / batch_size / (epoch+(i_batch+1)/total_step)

            if nomalized_time_per_batch > normalized_max_time_per_batch and i_batch > 10:
                logging.info('ooo' * 40)
                logging.info('training too slow, return... {}'.format(nomalized_time_per_batch))
                logging.info('ooo' * 40)
                return earlyOut("training too slow")

            if (i_batch + 1) % 100 == 0:
                logging.info('Epoch [{}/{}], Step [{}/{}], Loss: {:.4f}'.format(
                    epoch + 1, epochs_num, i_batch + 1, total_step, loss.item()))
        epoch_times.append(time.time() - epoch_start_time)

    train_time = time.time() - train_time
    logging.info('Training time for {} epoch(s): {}s'.format(epochs_num, train_time))

    # Validate the model
    logging.info('~+~' * 40)
    model.eval()

    valid_fraction = min(epochs_frac*2,1) if epochs_num == 1 else 1
    valid_score = eval(model, valid_loader, device, 'Validation', valid_fraction)
    test_score = eval(model, test_loader, device, 'Test') if epochs_num > 3 else 0
    status = "ok"
    return valid_score, test_score, train_time, status


def earlyOut(reason):
    return 0,0,0,reason


if __name__ == '__main__':
    logging.basicConfig(level=logging.DEBUG)
    valid_score, test_score, train_time, status = train(configuration.get_configuration())
