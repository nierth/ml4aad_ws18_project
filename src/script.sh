#!/bin/bash
#SBATCH --mem 16000 # memory pool for all cores 
#SBATCH -c 4 # number of cores
#SBATCH -t 0-23:59

echo "Workingdir: $PWD";
echo "Started at $(date)";
echo "Running job $SLURM_JOB_NAME using $SLURM_JOB_CPUS_PER_NODE cpus per node with given JID $SLURM_JOB_ID on queue $SLURM_JOB_PARTITION"; 

# Job to perform
python3 -u ~/ml4aad_ws18_project/src/bohb.py

# Print some Information about the end-time to STDOUT
echo "DONE";
echo "Finished at $(date)";
