import logging
import sys
import datetime
import traceback
import pprint
import os
import configuration

# simultaneously write to file and console
# class StreamToLogger(object):
#    """
#    Fake file-like stream object that redirects writes to a logger instance.
#    """
#    def __init__(self, logger, log_level=logging.INFO):
#       self.logger = logger
#       self.log_level = log_level
#
#    def write(self, buf):
#       for line in buf.rstrip().splitlines():
#          self.logger.log(self.log_level, line.rstrip())
#
#    filename = configuration.get_configuration()["general_log_dir"] + "sysout.log"
#    if not os.path.exists(os.path.dirname(filename)):
#          os.makedirs(os.path.dirname(filename))
#
#    logging.basicConfig(
#       level=logging.DEBUG,
#       format='%(message)s',
#       handlers=[logging.FileHandler(filename),
#                 logging.StreamHandler()])
#
#
# # redirect all prints to logger
# def redirectToLogger():
#    stdout_logger = logging.getLogger('stdout')
#    sl = StreamToLogger(stdout_logger, logging.DEBUG)
#    sys.stdout = sl
#
#    stderr_logger = logging.getLogger('stderr')
#    sl = StreamToLogger(stderr_logger, logging.ERROR)
#    sys.stderr = sl


# store errorenous configuration in case of an exception
def logException(cfg, ex):
   filename = cfg["general_log_dir"] + datetime.datetime.now().strftime("%Y-%m-%d_%H-%M-%S-%f") + ".txt"

   if not os.path.exists(os.path.dirname(filename)):
         os.makedirs(os.path.dirname(filename))

   with open(filename, "w") as fout:
      fout.write(traceback.format_exc())
      fout.write("\n")
      fout.write(pprint.pformat(cfg))
