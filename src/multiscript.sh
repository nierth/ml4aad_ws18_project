#!/bin/bash
#SBATCH --mem 16000 # memory pool for all cores (4GB)
#SBATCH -t 0-23:59 # time (D-HH:MM)
#SBATCH -c 1 # number of cores
#SBATCH -a 1-4 # array size


if [ 1 -eq $SLURM_ARRAY_TASK_ID ]; then
   python3 -u ~/ml4aad_ws18_project/src/bohb.py -l ./logs1/ -d K49
   exit $?
fi


if [ 2 -eq $SLURM_ARRAY_TASK_ID ]; then
   python3 -u ~/ml4aad_ws18_project/src/bohb.py -l ./logs2/ -d K49
   exit $?
fi


if [ 3 -eq $SLURM_ARRAY_TASK_ID ]; then
   python3 -u ~/ml4aad_ws18_project/src/bohb.py -l ./logs3/ -d K49 
   exit $?
fi


if [ 4 -eq $SLURM_ARRAY_TASK_ID ]; then
   python3 -u ~/ml4aad_ws18_project/src/bohb.py -l ./logs4/ -d K49  
   exit $?
fi